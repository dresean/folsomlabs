# Overview

Folsom Labs has decided to pivot into a travel planning startup.

To that end, we've specced out a class (`TravelGraph`) for managing our application state. The graph tracks nodes that each have a name, a price, and a list of destination nodes that you can reach from that location.

The `TravelGraph` class included already has functional implementations for core application functionality (adding, editing nodes). Your task is to provide implementations for some of the remaining methods in `traveler.js` as specified below.

We've included a test suite that will output test results as you complete subsequent problem sections.

*We expect this problem to take you around 2 hours. Please do NOT spend more than 3 hours on it.*


# The Problem
We have broken up the problem into three sections *(note: Part 3 is optional, do it if you have extra time)*:

## Part 1: Basic state management
1. Implement the `delete_node` function. This should both remove a node from the graph, and remove it from the destinations of all other nodes in the graph. Deleting a nonexistent node in the graph should throw a `ReferenceError`.
> this will be tested by the `TestStateChanges` test suite
> to run this, use: `npm run test-1`

2. Implement `undo` functionality. We should be able to undo the `add_node`, `edit_node`, and `delete_node` actions.
> this will be tested by the `TestBasicUndo` test suite
> to run this, use: `npm run test-1b`

## Part 2 - Multiple Undo and Redo
Implement multiple undo/redo within `TravelGraph`
* each call to undo in a row should revert one further action
    - n undo calls in a row should revert the graph to the state it had n actions ago, where actions are add_node, edit_node, or delete_node.
* each time a `undo` is called, a `redo` should be able to be called to revert the application state to before the initial undo (i.e. if `undo` has been called multiple times, you should be able to `redo` the same number of times)
* any change to the state (e.g. `add_node`, `edit_node` or `delete_node`) should remove potential redos until `undo` has been called again
* if there is not a state available for undo (or for redo) when one of the methods is called, the method should throw an `Error`
> this will be tested by the `TestUndoRedoStack` test suite
> to run this, use: `npm run test-2`


## Part 3 - Graph Traversal
Implement the graph traversal functions based on the data within `TravelGraph`
> this will be tested by the `TestGraphAnalysis` test suite


# Run locally:
To run the main `traveller.js` file, you can use these commands:

```
$ npm install        // install dependencies
$ npm test           // run tests
$ node traveller.js  // run main class file
```

You're not expected to add your own tests, but feel free to if it helps you implement. You will not be evaluated on whether your tests pass or fail.
