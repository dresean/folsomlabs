class NotImplementedError extends Error {
    constructor(params) {
        super(params);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, NotImplementedError);
        }
    }
}

// export default NotImplementedError;

module.exports = {
	NotImplementedError
};
