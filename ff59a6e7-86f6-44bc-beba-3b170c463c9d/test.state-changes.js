var TravelGraph = require('./traveller');
var assert = require('chai').assert;

describe('TestStateChanges', function() {
    var tg = new TravelGraph();

    beforeEach(function() { // before every it in every describe
        tg.setData([]);
    });

    describe('#addItem()', function() {
        it('should include the new node in graph items', function() {
            var p1 = tg.addItem('P1', 50, []);

            assert.equal(tg.items.length, 1);
            assert.equal(tg.getItem('P1'), p1);

            var p2 = tg.addItem('P2', 100, ['P1']);
            assert.equal(tg.items.length, 2);
            assert.deepStrictEqual(p2.destinations, [p1]);
        });
    });

    describe('#editItem()', function() {
        it('should have the updated node data', function () {
            var p1 = tg.addItem('P1', 50, []);
            var p2 = tg.addItem('P2', 100, ['P1']);

            tg.editItem(p1, 'P1 Edited', 150, ['P2']);

            assert.equal(p1.name, 'P1 Edited');
            assert.equal(p1.price, 150);
            assert.deepStrictEqual(p1.destinations, [p2]);
        });
    });

    describe('#deleteItem()', function() {
        it('should have node removed from items and all destinations', function () {
            var p1 = tg.addItem('P1', 50, []);
            var p2 = tg.addItem('P2', 100, ['P1']);
            var p3 = tg.addItem('P3', 150, ['P1', 'P2']);
            tg.editItem(p1, p1.name, p1.price, ['P2']);

            tg.deleteItem(p2);

            assert.equal(tg.items.length, 2);
            assert.equal(tg.items.indexOf(p2), -1);

            assert.equal(p1.destinations.length, 0);
            assert.deepStrictEqual(p3.destinations, [p1]);

            assert.throws(() => tg.getItem('P2'), ReferenceError);
        });

        it('should not be able to delete a nonexistent node', function () {
            var p1 = tg.addItem('P1', 50, []);
            var p2 = tg.addItem('P2', 100, ['P1']);

            tg.deleteItem(p2);
            assert.throws(() => tg.deleteItem(p2), ReferenceError);
        });
    });
});


