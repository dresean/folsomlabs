var NotImplementedError = require('./errors').NotImplementedError;
const util = require('util')
class TravelGraph {
    constructor(items=[]) {
        this.setData(items);
        this.history = []
    }
    // log current graph nodes to the console
    display(label='–') {
        console.log(`–– ${(label + ' ').padEnd(46,'–')}`);
        for(const { name, price, destinations } of this.items) {
            const destList = destinations.map(d => d.name).join(', ');
            console.log(`${name} (\$${price}) => [${destList}]`);
        }
        console.log('=================================================');
    }

    setData(items) {
        this.items = items;
    }

    getItem(name, items=this.items) {
        for (const item of items) {
            if (item.name === name) return item;
        }

        return null;
    }

    // add a new location item
    addItem(name, price, destinationNames) {
        this.history.push(this.items.splice())
        const destinations = destinationNames.map(name => this.getItem(name));

        const item = { name, price, destinations };
        this.items.push(item);
        this.history.push(this.items.splice())
        return item;
    }

    // change a given location item's properties
    editItem(item, name, price, destinationNames) {
        const destinations = destinationNames.map(name => this.getItem(name));
        Object.assign(item, { name, price, destinations });
        this.history.push(this.items.splice())
        return item;
    }

    // delete a location item from list
    deleteItem(item) {
        const items = this.items
        const { name, price, destinations } = item
        for( const object in items) {
           if(items[object]['name'] === item.name) {
            const index = items.indexOf(items[object])
            this.history.push(items.slice())
            items.splice(index, 1)
           this.history.push(items.slice())
           return 
           }
        }
        return new ReferenceError('No item with that name found')
    }


    // undo a change
    undoChange() {
        if(this.history.length < 1) return new Error('No history available')
        Object.assign(this.items, this.history[this.history.length - 2])
        this.history.push(this.items.slice())
    }

    // redo a change
    redoChange() {
        if(this.history.length < 1) return new Error('No history available')
       Object.assign(this.items, this.history[this.history.length - 2])
       this.history = this.history.splice(this.history.indexOf(this.history.length - 2), 1)
    }

    // clone all items in the current graph
    cloneItems(items = this.items || []) {}

    /**
     * Part 3: Graph Traversal (Optional) methods below
    */
    // return list of all destinations reachable from A
    allDestinations(itemA) {
        // throw new NotImplementedError();
        const { name, price, destinations } = itemA
        const destinations = this.getItem(itemA.name)
    }

    // return list of all origins which can reach A
    allOrigins(itemA) {
        // throw new NotImplementedError();
    }

    // return whether or not valid path exists from A to B
    validPath(itemA, itemB) {
        // throw new NotImplementedError();
    }

    // return list of locations on cheapest path to get from A to B
    cheapestPath(itemA, itemB) {
        // throw new NotImplementedError();
    }

    // serialize list to json string
    serialize() {}

    // deserialize json string to list
    deserialize(json) {}


    static sampleData() {
        const items = [
            { name: 'Fresno', price: 199 },
            { name: 'Toledo', price: 299 },
        ];

        Object.assign(items[0], {
            destinations: [items[1]],
        });

        Object.assign(items[1], {
            destinations: [items[0]],
        });

        return items;
    }
}


if (require.main === module) {
    var tg = new TravelGraph(TravelGraph.sampleData());
    tg.display('Initial Travel Graph')

    const modesto = tg.addItem('Modesto', 99, ['Fresno', 'Toledo']);
    tg.display('Add Modesto'); // Modesto should show up w/ Two Destinations

    const fresno = tg.getItem('Fresno');
    tg.deleteItem(fresno);
    tg.display('Delete Fresno'); // Fresno should be removed from the list

    tg.undoChange();
    tg.display('Undo Fresno Delete'); // Fresno should be fully back in the list

    tg.redoChange();
    tg.display('Redo Fresno Delete') // Fresno should be gone still, action before Fresno deletion taken again

}

module.exports = TravelGraph;
