var TravelGraph = require('./traveller');
var assert = require('chai').assert;

describe('TestUndoRedoStack', function() {
    var tg = new TravelGraph();

    beforeEach(function() {
        tg.setData([]);
    });

    describe('#undoChange(), #redoChange', function() {
        it('should undo multiple actions', function() {
            let p1 = tg.addItem('P1', 50, []);
            let p2 = tg.addItem('P2', 100, ['P1']);

            tg.deleteItem(p2);
            tg.editItem(p1, 'P1 Edited', p1.price, p1.destinations.map(d => d.name));

            tg.undoChange(); // undo edit

            p1 = tg.getItem('P1');
            assert.equal(p1.name, 'P1');
            assert.equal(tg.items.length, 1);

            tg.undoChange(); // undo delete

            p1 = tg.getItem('P1');
            p2 = tg.getItem('P2');
            assert.deepStrictEqual(tg.items, [p1, p2]);
            assert.deepStrictEqual(p2.destinations, [p1]);
        });

        it('should redo a single undo', function() {
            let p1 = tg.addItem('P1', 50, []);
            let p2 = tg.addItem('P2', 100, ['P1']);
            tg.editItem(p1, p1.name, p1.price, ['P2']);

            tg.undoChange(); // undo edit
            p1 = tg.getItem('P1');
            assert.deepStrictEqual(p1.destinations, []);

            tg.redoChange(); // redo edit

            p1 = tg.getItem('P1');
            p2 = tg.getItem('P2');
            assert.deepStrictEqual(p1.destinations, [p2]);
        });

        it('should clear potential redos after a new action', function() {
            var p1 = tg.addItem('P1', 50, []);
            var p2 = tg.addItem('P2', 100, ['P1']);

            tg.undoChange(); // undo P2 add

            var p3 = tg.addItem('P3', 150, ['P1']);
            assert.throws(() => tg.redoChange(), Error);
        });
    });
});
