var TravelGraph = require('./traveller');
var assert = require('chai').assert;


describe('TestBasicUndo', function() {
    var tg = new TravelGraph();

    beforeEach(function() {
        tg.setData([]);
    });

    describe('#undoChange()', function() {
        it('should undo a deletion', function() {
            let p1 = tg.addItem('P1', 50, []);
            let p2 = tg.addItem('P2', 100, ['P1']);
            let p3 = tg.addItem('P3', 150, ['P1', 'P2']);

            tg.deleteItem(p2);
            tg.undoChange();

            p1 = tg.getItem('P1');
            p2 = tg.getItem('P2');
            p3 = tg.getItem('P3');

            assert.deepStrictEqual(tg.items, [p1, p2, p3]);
            assert.deepStrictEqual(p3.destinations, [p1, p2]);
        });

        it('should undo a deletion and a mutation', function() {
            let p1 = tg.addItem('P1', 50, []);
            let p2 = tg.addItem('P2', 100, ['P1']);
            let p3 = tg.addItem('P3', 150, ['P1', 'P2']);

            tg.deleteItem(p2);
            tg.undoChange();
            assert.equal(tg.items.length, 3);

            var destinationNames = p1.destinations.map(d => d.name);
            tg.editItem(p1, 'P1 Edited', p1.price, destinationNames);
            tg.undoChange();

            p1 = tg.getItem('P1');
            p2 = tg.getItem('P2');
            p3 = tg.getItem('P3');

            assert.equal(p1.name, 'P1');
            assert.deepStrictEqual(tg.items, [p1, p2, p3]);
            assert.deepStrictEqual(p3.destinations, [p1, p2]);
        });

        it('should still undo when an action throws an error', function () {
            let p1 = tg.addItem('P1', 50, []);
            let p2 = tg.addItem('P2', 100, ['P1']);

            tg.deleteItem(p2);
            assert.throws(() => tg.deleteItem({}), ReferenceError);

            tg.undoChange(); // p2 should be re-added to graph
            p1 = tg.getItem('P1');
            p2 = tg.getItem('P2');

            assert.deepStrictEqual(tg.items, [p1, p2]);
        });
    });
});
